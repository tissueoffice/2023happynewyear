var opacity = 0;
var intervalID = 0;



function fadeIn() {
    blur();
    clearInterval(intervalID);
    intervalID = setInterval(show, 100);

}
function fadeOut() {
    unblur();
    clearInterval(intervalID);
    intervalID = setInterval(hide, 50);
}


function hide() {
    var div = document.getElementById("PopupContainter");
    opacity = Number(window.getComputedStyle(div).getPropertyValue("opacity"));

    if (opacity > 0) {
        //Fade out 핵심 부분
        opacity = opacity - 0.1;
        div.style.opacity = opacity;
    }
    else {

        clearInterval(intervalID);
    }
}

function show() {
    var div = document.getElementById("PopupContainter");
    opacity = Number(window.getComputedStyle(div).getPropertyValue("opacity"));

    if (opacity < 1) {
        //Fade in 핵심 부분
        opacity = opacity + 0.1;
        div.style.opacity = opacity;
    }
    else {

        clearInterval(intervalID);
    }
}

function blur() {
    var logoImg = document.getElementById("logoImg");
    var mainImg = document.getElementById("mainImg");

    logoImg.style.filter = "blur(2px)";
    mainImg.style.filter = "blur(2px)";


}
function unblur() {
    var logoImg = document.getElementById("logoImg");
    var mainImg = document.getElementById("mainImg");

    logoImg.style.filter = "blur(0px)";
    mainImg.style.filter = "blur(0px)";

}

