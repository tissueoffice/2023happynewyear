var isClickable = true;
var isPopupOn = false;
var audio = new Audio('popSound.wav');



function onClick() {
    //onclick -> changeImage+playSound -> (1s) ShowTextPopup -> OnClick reset
    if (!isClickable) return;

    if (isPopupOn) {
        HidePopup();
    } else {
        ShowPopup();
    }

}

function ShowPopup() {
    isPopupOn = true;
    isClickable = false;
    changeImage();
    playSound();

    setTimeout(function () {
        showTextPopup();
        isClickable = true;
    }, 1200);
}

function HidePopup() {
    isPopupOn = false;
    isClickable = false;
    changeImage();
    stopSound();
    hideTextPopup();
    isClickable = true;
}



function changeImage() {
    var mainImg = document.getElementById('mainImg');

    if (mainImg.src.match("tissue-scene.png")) {

        document.getElementById("mainImg").src = "tissue-scene1.png";
        mainImg.style.animationPlayState = "paused";
    } else {
        document.getElementById("mainImg").src = "tissue-scene.png";
        mainImg.style.animationPlayState = "running";
    }
}


function playSound() {
    audio.currentTime = 0;
    audio.play();
}

function stopSound() {
    audio.pause();
    audio.currentTime = 0;

}


function showTextPopup() {

    var textPopup = document.getElementById('popupText');
    textPopup.innerText = showCurrentText();
    fadeIn();
    addCurrentTextNum();

}

function hideTextPopup() {
    fadeOut();
}